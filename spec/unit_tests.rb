require 'rspec'
require 'wordpath'

DICT_PATH = '/usr/share/dict/words'.freeze

describe Node do
  it 'creates a Node object' do
    node = Node.new('foo')

    expect(node.to_s).to eq 'foo'
  end
end

describe Graph do
  it 'creates a graph with two nodes and one edge' do
    graph = Graph.new
    nodes = [
      graph.node('foo'),
      graph.node('bar')
    ]

    graph.add_edge(nodes[0], nodes[1])

    expect(graph.nodes['foo']).to eq nodes[0]
    expect(graph.nodes['bar']).to eq nodes[1]
    expect(graph.nodes.keys.length).to eq 2
  end
end

describe GraphBuilder do
  it 'builds a graph from the default words file' do
    g = GraphBuilder.new(DICT_PATH, 'cat', 'dog').graph

    expect(g.nodes['cat'].to_s).to eq 'cat'
    expect(g.nodes['dog'].to_s).to eq 'dog'
    expect(g.nodes['fear'].to_s).to eq ''
  end
end

describe BFS do
  it 'finds a path between cat and dog' do
    start_word = 'cat'
    end_word   = 'dog'

    graph = GraphBuilder.new(DICT_PATH, start_word, end_word).graph
    wp = BFS.new(graph, graph.node(start_word), graph.node(end_word)).path

    # The first node in our path should be our start word.
    expect(wp.first.to_s).to eq start_word

    # And the last should be our end word.
    expect(wp.last.to_s).to eq end_word
  end

  it 'does not find a path between autumn and any other word' do
    # The word autumn has no valid transformations in the default dictionary file.
    start_word = 'autumn'
    end_word   = 'flower'

    expect { GraphBuilder.new(DICT_PATH, start_word, end_word).graph }.to raise_error(PathError)
  end
end
