# Readme

Calculates paths between two words by making single letter substitut-
ions.

All of the relevant code is in the lib directory. The bin
directory contains just enough code to parse command line arguments
and start a wordpath search.

The spec dirctory contains some basic rspec tests. Use ```rspec
spec/unit_tests.rb to run the test suite.```

The cli component in bin takes three arguments. The path to the dic-
tionary file, the start word, and the end word. Start and end word
must be the same length.

The dictionary file should be formatted similarly to the one in
`/usr/share/dict/words` on *nix systems.
