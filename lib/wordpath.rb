#!/usr/bin/env ruby
require 'set'

class ArgumentLengthError < ArgumentError
  def initialize(msg = 'The given start and end word must have the same number of characters.')
    super
  end
end

class InvalidWord < ArgumentError
  def initialize(msg)
    super("'#{msg}'' not found in dictionary")
  end
end

class PathError < StandardError
  def initialize(start_word, end_word)
    super("there is no valid path between '#{start_word}' and '#{end_word}'")
  end
end

class Node
  attr_accessor :adjacents

  def initialize(name)
    @adjacents = Set.new
    @name = name
  end

  def to_s
    @name
  end
end

class Graph
  attr_reader :nodes

  def initialize
    @nodes = {}
  end

  # Returns the node with the name n, or creates that node if it does
  # not exist
  def node(n)
    @nodes[n] = Node.new(n) if @nodes[n].nil?
    @nodes[n]
  end

  def add_edge(node_a, node_b)
    node_a.adjacents << node_b
    node_b.adjacents << node_a
  end
end

# Builds our graph for our BFS algorithm using a given dictionary file
# and our start and end words.
class GraphBuilder
  attr_reader :graph

  CHARSET = ('a'..'z').to_a.join.freeze

  def initialize(file, start_word, end_word)
    @file       = file
    @start_word = start_word
    @end_word   = end_word

    raise ArgumentLengthError if @start_word.length != @end_word.length

    @graph = Graph.new
    @nodes = {}
    @words = Set.new

    load_dictionary
    build_graph!
  end

  private

  def word?(w)
    @words.include? w
  end

  # Generate an array of all the valid words with a hamming distance
  # of exactly 1 from the given word.
  # This is vastly more efficient than comparing our word against
  # every other word in the dictionary.
  def generate_neighbours(word)
    neighbours = Set.new

    word.split('').each do |char|
      CHARSET.split('').each do |replacement|
        neighbour = word.sub(char, replacement)
        next if neighbour == word
        neighbours << neighbour if word?(neighbour)
      end
    end
    neighbours.to_a
  end

  def load_dictionary
    File.readlines(@file).each do |l|
      # Remove trailing newlines.
      l.chomp!
      @words << l.downcase if l.length == @start_word.size
    end

    raise InvalidWord.new(@start_word) unless word?(@start_word)
    raise InvalidWord.new(@end_word) unless word?(@end_word)
  end

  # We build a minimal undirected graph where each node is a word in
  # our dictionary and the edges link nodes to the nearest neighbours.

  # We build our graph using a queue. First we push our starting node
  # into the queue. Then while the queue still has elements, we
  # take the first element from the queue and delete its word from
  # the dictionary, generate all its neighbours with a hamming
  # distance of 1, and push those neighbours into our work queue.
  # We do this until we find our target node or the queue is empty.
  # This allows us to build a minimal graph of transformable words
  # stop as soon as we know a word ladder exists.
  def build_graph!
    queue = []
    queue << @start_word

    while queue.any?
      current_word = queue.shift
      @words.delete current_word

      current_neighbours = generate_neighbours(current_word)

      current_neighbours.each do |n|
        @graph.add_edge(@graph.node(current_word), @graph.node(n))
      end

      current_neighbours.each do |n|
        queue << n unless queue.include? n
      end

      return if current_word == @end_word
    end
    raise PathError.new(@start_word, @end_word)
  end
end

# BFS performs a simple breadth-first search of a given graph,
# starting node and destination node.
class BFS
  attr_reader :path
  def initialize(graph, start_node, end_node)
    @graph = graph
    @start_node = start_node
    @end_node = end_node

    @visited = []
    @edge_to = {}

    bfs(@start_node)
    @path = path_to(@end_node)
  end

  private

  def bfs(node)
    queue = []
    queue << node
    @visited << node

    # We visit each of our nodes and their adjacents, marking them
    # as we go and using the edge_to hash to track the path we take.
    # We know the graph will contain both our start point and end
    # due to the method of its construction.
    while queue.any?
      current_node = queue.shift

      current_node.adjacents.each do |adj_node|
        next if @visited.include?(adj_node)
        queue << adj_node
        @visited << adj_node
        @edge_to[adj_node] = current_node
      end
    end
  end

  # Returns the worth path taken as an array of nodes
  def path_to(node)
    path = []

    while node != @start_node
      path.unshift(node)
      node = @edge_to[node]
    end

    path.unshift(@start_node)
  end
end
